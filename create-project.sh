#!/bin/bash

www_path="www";

nginx_dir="nginx/conf.d/";

ssl_dir="nginx/ssl/";


read -p "Название домена: " domain


if [ -n "$domain" ]; then

	read -p "Создать дерикторию в каталоге 'www' (y/n): " create_directory

	if [[ "$create_directory" == "y" ]]; then

		if ! [ -d $www_path/$domain/ ]; then

			mkdir $www_path/$domain;

		else

			echo "Директория уже существует!";

		fi

	fi

	read -p "Создать конфиг Nginx (y/n): " create_config_nginx

	if [[ "$create_config_nginx" == "y" ]]; then

 echo "server {

    listen 80;

    listen 443 ssl;

    index index.php index.htm index.html;

    server_name $domain;

    error_log  /var/log/nginx/error.log;
 
    access_log /var/log/nginx/access.log;

    root /var/www/$domain;

    ssl_certificate /etc/nginx/ssl/$domain.crt;

    ssl_certificate_key /etc/nginx/ssl/device.key;

    client_max_body_size 100m;

    location ~ \.php$ {

        try_files \$uri =404;

        fastcgi_split_path_info ^(.+\.php)(/.+)$; 

        fastcgi_pass php:9000;

        fastcgi_index index.php;

        include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;

        fastcgi_param PATH_INFO \$fastcgi_path_info;

    }


    location / {

        try_files \$uri \$uri/ /index.php?\$args;

    }
}
 " > $nginx_dir/$domain.conf

	fi

	read -p "Создать ssl сертификат (y/n): " create_ssl

	if [[ "$create_ssl" == "y" ]]; then

		if [ -f $ssl_dir/device.key ]; then

		  KEY_OPT="-key"

		else

		  KEY_OPT="-keyout"

		fi

		DOMAIN=$domain

		COMMON_NAME=${2:-$domain}

		SUBJECT="/C=CA/ST=None/L=NB/O=None/CN=$COMMON_NAME"

		NUM_OF_DAYS=999

		openssl req -new -newkey rsa:2048 -sha256 -nodes $KEY_OPT $ssl_dir/device.key -subj "$SUBJECT" -out device.csr

		cat .gssl/v3.ext | sed s/%%DOMAIN%%/$COMMON_NAME/g > /tmp/__v3.ext

		openssl x509 -req -in device.csr -CA .gssl/rootCA.pem -CAkey .gssl/rootCA.key -CAcreateserial -out device.crt -days $NUM_OF_DAYS -sha256 -extfile /tmp/__v3.ext

		mv device.csr $ssl_dir/$DOMAIN.csr

		cp device.crt $ssl_dir/$DOMAIN.crt

		# remove temp file
		rm -f device.crt;

	fi

	echo "Сайт успешно создан! Не забудьте прописать адрес домена в hosts файл!";

else
	echo "Вы не ввели имя домена";
fi